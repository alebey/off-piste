from argparse import ArgumentParser
from logging import getLogger
from os import getenv

from gnar_gear import GnarApp

log = getLogger()

parser = ArgumentParser()
parser.add_argument('-p', '--port', type=int)
args = parser.parse_args()
port = vars(args)['port'] or 80
production = port == 80 and getenv('GNAR_ENV', '') != 'development'
origin = 'https://app.gnar.ca' if production else 'http://localhost:3000'

if __name__ == '__main__':
    GnarApp('off-piste', production, port, no_db=True, no_jwt=True).run()
